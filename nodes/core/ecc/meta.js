module.exports = function(RED) {
    function BotConfiguration(config) {
        RED.nodes.createNode(this,config);
        var node = this;
	    this.on('input', function(msg) {
		
			 node.send(msg);
        });
    }
    RED.nodes.registerType("BotConfiguration",BotConfiguration);
}