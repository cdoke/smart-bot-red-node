module.exports = function(RED) {
    function SelectBot(config) {
        RED.nodes.createNode(this,config);
        var node = this;
            this.on('input', function(msg) {
                        //console.log(config);
                        msg.selectedBot = config.selectedVal;
                        var selectedBot = config.selectedVal;
                        var url = "http://localhost:8084/flow/"+selectedBot;
                                        var request = require("request");
        
                var options ={
                                                        method: 'GET',
                                                        url: url,
                                                        headers: 
                                                        { 
                                                                 'postman-token': '3a026a98-e8dc-995c-e78c-18456340c3c3',
                                                                'cache-control': 'no-cache',
                                                                authorization: 'Basic dXNlcjpQYXNzd29yZEAxMjM='
                                                        },
                                                                body: '{"name": "Hello","id":'+selectedBot+'}' 
                                        };

                                request(options, function (error, response, body) {
                                  if (error) {throw new Error(error);}
                                  else{
                                           
                                                                                   var obj = JSON.parse(response.body);
                                                                                //  console.log("::::::::::::::::"+obj);
                                           obj = obj.nodes;
                                           //console.log(obj)
                                                /*---------------------------------*/
                                                        var routes=[];
                                                        var init = '';
                                                        var db = {};
                                                       function getJsonMeta() {
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'BotConfiguration') {
                                                                        var object = {};
                                                                        object._id = item._id;
                                                                        object.description = item.Description;
                                                                        object.name = item._id;
                                                                        object.__v = item.__v;
                                                //            db.push(object);
                                                                        db = object;
                                                                }
                                                        });
                                                        getChannels();
                                                }
                                                
                                                function getChannels() {
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'Channels') {
                                                                       var channels = {};
                                                                        channels.id = item.Id;
                                                                        if (item.skype) {
                                                                                channels.skype = item.skype;
                                                                        }
                                                                        if (item.telegram) {
                                                                                channels.telegram = item.telegram;
                                                                        }
                                                                        if (item.facebook) {
                                                                                channels.facebook = item.facebook;
                                                                        }
                                                                        if (item.kik) {
                                                                                channels.kik = item.kik;
                                                                        }
                                                                        if (item.slack) {
                                                                                channels.slack = item.slack;
                                                                        }
                                                                        if (item.sms) {
                                                                                channels.sms = item.sms;
                                                                        }
                                                                        db.channels = channels;
                                                //            db.push('/channels', channels);
                                                                }
                                                        });
                                                        getTemplate();
                                                }

                                                function getTemplate() {
                                                        var template = {};
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'PrimitiveNode') {
                                                                        template._id = item._id;
                                                                        template.initialRoute = item.initialRoute;
                                                                        init = item.initialRoute;
                                                                        obj.forEach(function(item, index) {
                                                                                if (item.type === 'GlobalResponse') {
                                                                                        template.middleware = {
                                                                                        'hiPattern': item.hiPattern,
                                                                                        'byePattern': item.byePattern,
                                                                                        'helpPattern': item.helpPattern};
                                                                                        db.template = template;
                                                //                    db.push('/template', template);
                                                                                }
                                                                        });
                                                                }
                                                        });
                                                        getAllRoutes();
                                                }

                                                function getAllRoutes() {
                                                        var nextRoutes = [];
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'ButtonRoute') {
                                                                        var route = {};
                                                                        var value = [];
                                                                        var value2 = [];
                                                                        var btnArr = [];

                                                                        route.routeName = item.name;

                                                                        value.push(item.prompt);
                                                                        if (item.varName) {
                                                                                route.varName = item.varName;
                                                                        } else {
                                                                                route.varName = 'Response';
                                                                        }
                                                                        route.type = 'attachment';
                                                                        item.wires.forEach(function (rId) {
                                                                                if (rId[0]) {
                                                                                        obj.forEach(function (item) {
                                                                                        if (item.id === rId[0]) {
                                                                                                nextRoutes.push(item.name);
                                                                                        }
                                                                                        });
                                                                                } else {
                                                                                        nextRoutes.push(init);
                                                                                }
                                                                        });
                                                                        //console.log(nextRoutes);
                                                                        var valueObj = {};
                                                                        valueObj.type = 'button';

                                                                        item.rules.forEach(function (btns, i) {
                                                                                btns.type = btns.routeType;
                                                                                btns.nextRoute = nextRoutes[i];
                                                                                btnArr.push(btns);
                                                                        });
                                                                        value2.push({'buttons': btnArr});
                                                                        valueObj.value = value2;
                                                                        value.push(valueObj);
                                                                        route.value = value;
                                                                        routes.push(route);
                                                                }
                                                        });
                                                        getCarouselRoutes();
                                                        getTextRoutes();
                                                }
                                                function getCarouselRoutes() {
                                                        var nextRoutes = [];
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'Carousel') {
                                                                        var route = {};
                                                                        var value = [];
                                                                        var value2 = [];

                                                                        route.routeName = item.name;
                                                                        route.prompt = item.prompt;
                                                                        if (item.varName) {
                                                                                route.varName = item.varName;
                                                                        } else {
                                                                                route.varName = 'Response';
                                                                        }
                                                                        route.type = 'attachment';
                                                                        item.wires.forEach(function (rId) {
                                                                                if (rId[0]) {
                                                                                        obj.forEach(function(item) {
                                                                                        if (item.id === rId[0]) {
                                                                                                nextRoutes.push(item.name);
                                                                                        }
                                                                                        });
                                                                                } else {
                                                                                        nextRoutes.push(init);
                                                                                }
                                                                        });
                                                                        //console.log(nextRoutes);
                                                                        var valueObj = {};
                                                                        valueObj.type = 'carousel';
                                                                        item.rules.forEach(function (crsl, i) {
                                                                                var carousel = {};
                                                                                var btn = {};
                                                                                carousel.imageUrl = crsl.imageurl;
                                                                                carousel.text = crsl.textmsg;
                                                                                                                                        if(crsl.title){
                                                                                btn.title = crsl.title;
                                                                                btn.value = crsl.value;
                                                                                btn.nextRoute = nextRoutes[i];
                                                                                btn.type = crsl.routeType;
                                                                                carousel.buttons = [];
                                                                                carousel.buttons.push(btn);
                                                                                                                                                        }
                                                                                value2.push(carousel);
                                                                        });

                                                                        valueObj.value = value2;
value.push(route.prompt);
                                                                        value.push(valueObj);
                                                                       route.value = value;
                                                                        routes.push(route);

                                                                }
                                                        });
                                                }
                                                function getTextRoutes() {
                                                        obj.forEach(function (item) {
                                                                if (item.type === 'chatbot-message') {
                                                                        var route = {};
                                                                        route.routeName = item.name;
                                                                        route.prompt = item.message[0].message;
                                                                        route.varName = 'Response';
                                                                        route.type = 'text';
                                                                        obj.forEach(function (items) {
                                                                                if (item.wires[0][0] && item.wires[0][0] === items.id) {
                                                                                        route.nextRoute = items.name;
                                                                                } else {
                                                                                        route.nextRoute = init;
                                                                                }
                                                                        });
                                                                        routes.push(route);
                                                                }
                                                        });
                                                        db.template.routes = routes;
                                                //    console.log(db);
                                                //    db.push('/template/routes/', routes);
                                                }
                                                getJsonMeta();

                                                /*------------------------------------*/
                                                msg.res = db;
                                                //var body = JSON.strigi
                                                //console.log(db);
                                                var Botoptions ={
                                                        method: 'POST',
                                                        url: 'https://dev.smartbothub.com/5000/createBot',
                                                        headers: 
                                                        { 
                                                                //* 'postman-token': '3a026a98-e8dc-995c-e78c-18456340c3c3',
                                                                /*'cache-control': 'no-cache',
                                                                authorization: 'Basic dXNlcjpQYXNzd29yZEAxMjM='*/
                                                                'content-type': 'application/json'
                                                        },
                                                  body: JSON.stringify(db)
                                        };
                                
                                request(Botoptions, function (error, response, body) {
                                  if (error) {throw new Error(error);}
                                else{
                                        //console.log(response);
                                                                                        msg.response = body;
                                                                                        //console.log(":::::::::::::::"+response);
                                                                                        var res = JSON.parse(body);
                                                                                        
                                       // node.send(msg);
                                                                           
                                                                                 var url = "https://dev.smartbothub.com/4005/refreshBot/"+res.id;
                                                                                        
                                                                                         console.log("URL:::::::::::"+url);
                                                                                        
                                                                                         /*-----------------*/
                                                                                        
                                                                                        var options ={
                                                                                                                method: 'GET',
                                                                                                                url: url,
                                                                                                                body: '{"id":'+res.id+'}' 
                                                                                                };

                                                                                        request(options, function (error, response, body) {
                                                                                           if (error) {throw new Error(error);}
                                                                                        else{
                                                                                                
                                                                                                //console.log("hello----- "+JSON.stringify(response));
                                                                                                msg.refreshStatus = response.body;
                                                                                                node.send(msg);
                                                                                        }
                                                                                        })
                                                                /*-------------------*/
                                }})
                                                
                                           
                                 
                }
                                
                });

                        
        });
    }
    RED.nodes.registerType("SelectBot",SelectBot);
}

