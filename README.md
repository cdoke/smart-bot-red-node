# ECC

## Quick Start

If you want to run the latest code from git, here's how to get started:

1. Clone the code:

        git clone https://cdoke@bitbucket.org/cdoke/smart-bot-red-node.git
       


2. Build the code

        npm run build

3. Run

        npm start
   or

        node red.js


## Getting Help

More documentation can be found [here](http://nodered.org/docs).

For further help, or general discussion, please use the
[mailing list](https://groups.google.com/forum/#!forum/node-red).